package strings;

/**
 * Created by a on 25.12.15.
 */
public class IndexOf {
    public static void main(String[] args) {
        String text = "qwertyuiop";
        String content = "tyu";
//        System.out.println(text.contains(content));

        char[] chars = content.toCharArray();

        for(char c : chars) {
            if(text.indexOf(c) == -1){
                System.out.println(false);
            }
        }
        System.out.println(true);
    }
}
