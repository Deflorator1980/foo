package strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameCheck {

    public static void main(String[] args){
        System.out.println(dumbCheck("_@BEST"));
        System.out.println(dumbCheck("voan"));
        System.out.println(dumbCheck("vo"));
        System.out.println(dumbCheck("Z@OZA"));
    }

    public static boolean dumbCheck(String userNameString){

        char[] symbols = userNameString.toCharArray();
        if(symbols.length < 3 || symbols.length > 15 ) return false;

        String validationString = "abcdefghijklmnopqrstuvwxyz0123456789_";

        for(char c : symbols){
            if(validationString.indexOf(c)==-1) return false;
        }

        return true;
    }
}
