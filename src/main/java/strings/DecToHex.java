package strings;


public class DecToHex {
    public static void main(String[] args) {
        int x = 232;
        System.out.println(Integer.toHexString(162));

        Integer a = new Integer(1);
        Integer b = new Integer(1);
        System.out.println(a == b);
        System.out.println(a.equals(b));

        String s1 = "s";
        String s2 = "s";
        String s3 = new String("s");
        System.out.println(s1 == s2);
        System.out.println(s2 == s3);
        System.out.println(s2.equals(s3));

    }
}
