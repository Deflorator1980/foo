package factorial;

import java.math.BigInteger;

public class Foo {
    public static void main(String[] args) {
        System.out.println(fac(new BigInteger("50")));
    }

    public static BigInteger fac (BigInteger x){
        return (x.equals(BigInteger.ZERO))? BigInteger.ONE : x.multiply(fac(x.subtract(BigInteger.ONE)));
    }
}