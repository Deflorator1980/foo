package enter_from_console;

/**
 * Created by a on 08.12.15.
 */
public class Enter {
    public static void main(String[] args) {
        while (true) {
            System.out.print("Enter something:");
            String input = System.console().readLine();
            System.out.println(input);
            if ("close".equals(input)){
                break;
            }
        }
        System.out.println("Finish!");
    }
}
