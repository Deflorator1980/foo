package enter_from_console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EnterBuffer {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter String:");
        String s = br.readLine();
        System.out.print("Enter Integer:");
        int i = 0;
        try{
            i = Integer.parseInt(br.readLine());
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }
        System.out.println(s);
        System.out.println(i);
    }
}
