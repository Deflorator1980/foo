package enter_from_console;

import java.util.Scanner;

/**
 * Created by a on 08.12.15.
 */
public class EnterScanner {
    public static void main(String[] args) {
        while (true) {
            System.out.print("Enter:");
            Scanner in = new Scanner(System.in);
            String s = in.next();
            System.out.println(s);
            if ("close".equals(s)){
                break;
            }
        }
    }
}
