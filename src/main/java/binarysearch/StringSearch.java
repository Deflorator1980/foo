package binarysearch;

import java.util.Arrays;

/**
 * Created by a on 21.01.16.
 */
public class StringSearch {
    public static void main(String[] args) {

        // initializing unsorted int array
        String stringArr[] = {"hui", "pesda", "blyad", "ipazo"};

        // sorting array
        Arrays.sort(stringArr);

        // let us print all the elements available in list
        System.out.println("The sorted int array is:");
        for (String string : stringArr) {
            System.out.println("Number = " + string);
        }

        // entering the value to be searched
        String searchVal = "pesda";

        int retVal = Arrays.binarySearch(stringArr, searchVal);

        System.out.println("The index of element " + searchVal+" is : " + retVal);
    }
}
