package binarysearch;

import java.util.Arrays;

/**
 * Created by a on 21.01.16.
 */
public class BS1 {

    public static int[] getArr(){
        int intArr[] = {30,20,5,12,55};
        Arrays.sort(intArr);
        return intArr;

    }
    public static int binarySearch(int[] array, int value, int left, int right) {

        if (left > right) return -1;

        int middle = (left + right) / 2;

        if (array[middle] == value) return middle;

        else if (array[middle] > value) return binarySearch(array, value, left, middle - 1);

        else return binarySearch(array, value, middle + 1, right);

    }

    public static void main(String[] args) {
        int[] arr = getArr();
        for(int i : arr) System.out.print(i + " ");
        System.out.println();
        System.out.println(binarySearch(arr, 30, 0, arr.length - 1));
    }
}
