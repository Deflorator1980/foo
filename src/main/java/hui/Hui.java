package hui;

/**
 * Created by a on 21.01.16.
 */
class Ancestor{
    int i;
}

class A extends Ancestor{
    int i = 10;
}

class B extends Ancestor{
    int i = 20;
}
public class Hui {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        A a1 = new A();
        System.out.println(a.i);
        a.i = 15;
        System.out.println(a.i);
        a1 = a;
        System.out.println(a1.i);
        System.out.println(a.hashCode());
        System.out.println(a1.hashCode());
    }
}
