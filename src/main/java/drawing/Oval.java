package drawing;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Oval extends JPanel {

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.yellow);
        g2.setStroke(new BasicStroke(10));
        g2.drawOval(10, 10, 220, 120);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(new Oval());

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setVisible(true);
        frame.setTitle("Java");
        frame.setBackground(new Color(0, 162, 232));
    }
}