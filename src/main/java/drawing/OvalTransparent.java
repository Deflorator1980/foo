package drawing;

import javax.swing.*;
import java.awt.*;

/**
 * Created by a on 26.12.15.
 */
public class OvalTransparent {
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.yellow);
        g2.setStroke(new BasicStroke(10));
        g2.drawOval(10, 10, 220, 120);
    }

    public static void main(String[] args) {
//        JFrame frame = new JFrame();
        JWindow frame = new JWindow();
        frame.setVisible(true);
        frame.getContentPane().add(new Oval());

//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setBackground(new Color(0, 162, 232));
//        frame.setTitle("Java");
    }
}
