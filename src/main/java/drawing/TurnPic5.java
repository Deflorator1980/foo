package drawing;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

public class TurnPic5 extends JFrame{
    static JPanel pizd, p1;
    ImageIcon ii1;
    AffineTransform at;
    static int xp1, yp1, wp1, hp1;
    static double hwp1;
    TurnPic5(){
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pizd = new JPanel();
        pizd.setBackground(new Color(0, 162, 232));
        setContentPane(pizd);
        at = new AffineTransform();
        ii1 = new ImageIcon("Кузя.jpg");
        wp1 = ii1.getIconWidth(); hp1 = ii1.getIconHeight();
        hwp1 = hp1*1.0/wp1;
        p1 = new JPanel(){
            public void paint(Graphics g){
                super.paint(g);
                Graphics2D g2 = (Graphics2D)g;
                g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                hp1 = (int)(hwp1*wp1);
                g2.setTransform(at);
                g2.drawImage(ii1.getImage(), 0, 0 , wp1, hp1, this);
                p1.setSize(wp1, hp1);
            }
        };
        p1.setPreferredSize(new Dimension(400, 300));
        p1.setBackground(Color.yellow);
        pizd.add(p1);
        p1.addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
                xp1 = e.getXOnScreen(); yp1 = e.getYOnScreen();
            }
        });
        p1.addMouseWheelListener(new MouseWheelListener(){
            public void mouseWheelMoved(MouseWheelEvent e){
                wp1 -= e.getUnitsToScroll()*3;
                p1.repaint();
            }
        });
        p1.addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
                p1.setLocation(p1.getX() + e.getXOnScreen() - xp1, p1.getY() + e.getYOnScreen() - yp1);
                xp1 = e.getXOnScreen(); yp1 = e.getYOnScreen();
            }
        });
        p1.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e){
                if(e.getKeyCode()==KeyEvent.VK_UP){
                    wp1 += 10;
                    p1.repaint();
                }
                if(e.getKeyCode()==KeyEvent.VK_DOWN){
                    wp1 -= 10;
                    p1.repaint();
                }
                if(e.getKeyCode()==KeyEvent.VK_LEFT){
                    at.rotate(-Math.PI/6, wp1/2, hp1/2);
                    p1.repaint();
                }
                if(e.getKeyCode()==KeyEvent.VK_RIGHT){
                    at.rotate(Math.PI/6, wp1/2, hp1/2);
                    p1.repaint();
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame f = new TurnPic5();
        f.setVisible(true);
        p1.requestFocus();
    }
}