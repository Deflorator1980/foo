package other;

public class My {
    static class MyThread extends Thread {
        public void run() {
            System.out.print("Thread ");
        }
    }

    public static void main(String[] args) {
        MyThread t = new MyThread();
        t.start();
        System.out.print("one. ");
        t.start();
        System.out.print("two. ");
        System.out.print("Thread ");
    }
}