package other;

final class SingleObject{
    private String s;

    private static SingleObject instance = new SingleObject();

    public static SingleObject getInstance() {
        return instance;
    }

    public void setValue(String value) {
        s = value;
    }

    public String getValue() {
        return s;
    }
}

public class SingletonPatternDemo {

    public static void main(String[] args) {
        SingleObject object = SingleObject.getInstance();
        object.setValue("hui");
        System.out.println(object.getValue());

        SingleObject object2 = SingleObject.getInstance();
        System.out.println(object2.getValue());

        object2.setValue("pisda");
        System.out.println(object.getValue());
    }
}
