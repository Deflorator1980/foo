package other;

import java.util.ArrayList;
import java.util.List;

final class SingleList{
    private List<String> s;

    public List<String> getS() {
        return s;
    }

    public void setS(List<String> s) {
        this.s = s;
    }

    public static void setInstance(SingleList instance) {
        SingleList.instance = instance;
    }

    private static SingleList instance = new SingleList();

    public static SingleList getInstance(){
        return instance;
    }
}
public class SingletonList {
    public static void main(String[] args) {
        SingleList s1 = SingleList.getInstance();
        s1.setS(getList("hui", "pisda", "blyad"));
        System.out.println(s1.getS());
        SingleList s2 = SingleList.getInstance();
        s2.setS(getList("hui2", "pisda2", "blyad2"));
        System.out.println(s1.getS());
    }

    public static List<String> getList(String one, String two, String three){
        List<String>list = new ArrayList<>();
        list.add(one);
        list.add(two);
        list.add(three);
        return list;
    }
}
