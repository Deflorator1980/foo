package qsort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by a on 30.12.15.
 */
public class PlatformSort {
    public static void main(String[] args) {
        List<String> huis = new ArrayList<>();
        huis.add("Hui");
        huis.add("Pisda");
        huis.add("Blyad");
        huis.add("Ebat");

        System.out.println(huis);

        String[] huiArray = huis.toArray(new String[huis.size()]);

        for(String s : huiArray) System.out.print(s + " ");
        System.out.println();

        Arrays.sort(huiArray);
        for(String s : huiArray) System.out.print(s + " ");

        System.out.println();
        Collections.sort(huis, ((o1, o2) -> o1.compareTo(o2)));
        System.out.println(huis);



    }
}
