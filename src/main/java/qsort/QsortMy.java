package qsort;

/**
 * Created by a on 23.12.15.
 */
public class QsortMy {

    public static int[] getSet() {
        int[] set = new int[70000];

        for (int i = 0; i <= set.length - 1; i++) {
            set[i] = (int) Math.round(Math.random() * 200);
        }
        return set;
    }

    public static int[] qsort(int first, int last, int[] set) {
        if (first >= last) return set;
        int i = first;
        int j = last;
        int op = (j - i) / 2 + i;
        while (i < j) {
            if ((i < op) && (set[i] <= set[op])) i++;
            if ((j > op) && (set[j] >= set[op])) j--;

            if (i < j) {
                int tmp = set[i];
                set[i] = set[j];
                set[j] = tmp;
                if (i == op) op = j;
                else if (j == op) op = i;
            }

        }
        qsort(first, op, set);
        qsort(op + 1, last, set);

        return set;
    }


    public static void main(String[] args) {
        int[] set = getSet();
        for (int i : set) System.out.print(i + " ");
        System.out.println();

        long startTime = System.nanoTime();
        set = qsort(0, set.length - 1, set);
        long stopTime = System.nanoTime();

        for (int i : set) System.out.print(i + " ");
        System.out.println();
        System.out.println((stopTime - startTime)/(1000000));

    }
}
