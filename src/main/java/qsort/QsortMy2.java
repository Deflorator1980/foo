package qsort;

/**
 * Created by a on 20.01.16.
 */
public class QsortMy2 {

    public static int[] getSet() {
        int[] set = new int[10];
        for (int i = 0; i < set.length; i++) {
            set[i] = (int)Math.round(Math.random()*20);
        }
        return set;
    }

    public static int[] qsort(int first, int last, int[] set) {
        if (first >= last) return set;
        int i = first;
        int j = last;
        int op = (j + i)/2;
        while (i < j) {
            if ((i < op) && (set[i] <= set[op])) i++;
            if ((j > op) && (set[j] >= set[op])) j--;

            if (i < j) {
                int tmp = set[i];
                set[i] = set[j];
                set[j] = tmp;
                if (i == op) op = j;
                else if (j == op) op = i;
            }
        }
        qsort(first, op, set);
        qsort(op + 1, last, set);
        return set;
    }

    public static void main(String[] args) {
//        int[] set = {2, 4, 1, 0, 15, 11, 14, 1};
        int[] set = getSet();
        for (int x : set) System.out.print(x + " ");
        System.out.println();
        set = qsort(0, set.length - 1, set);
        for (int x : set) System.out.print(x + " ");

    }
}
