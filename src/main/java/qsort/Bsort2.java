package qsort;

/**
 * Created by a on 20.01.16.
 */
public class Bsort2 {

    public static int[] getSet() {
        int[] set = new int[700];

        for (int i = 0; i <= set.length - 1; i++) {
            set[i] = (int) Math.round(Math.random() * 200);
        }
        return set;
    }

    public static int[] bsort(int[] set) {
        int j;
        boolean flag = true;
        int tmp;

        while (flag) {
            flag = false;
            for (j = 0; j < set.length - 1; j++) {
                if (set[j] > set[j + 1]) {
                    tmp = set[j];
                    set[j] = set[j + 1];
                    set[j + 1] = tmp;
                    flag = true;

                }
            }
        }
        return set;
    }

    public static void main(String[] args) {
//        int[] set = getSet();
        int[] set = {7, 16, 8, 9, 2, 6, 2};
        for (int i : set) System.out.print(i + " ");
        System.out.println();

        long startTime = System.nanoTime();
        set = bsort(set);
        long stopTime = System.nanoTime();

        for (int i : set) System.out.print(i + " ");
        System.out.println();
        System.out.println((stopTime - startTime) / (1000000));
    }
}
