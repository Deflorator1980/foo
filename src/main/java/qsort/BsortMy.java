package qsort;

/**
 * Created by a on 20.01.16.
 */
public class BsortMy {

    public static int[] getSet() {
        int[] set = new int[10];
        for (int i = 0; i < set.length; i++) {
            set[i] = (int)Math.round(Math.random()*20);
        }
        return set;
    }

    public static int[] bsort(int[] set) {
        boolean flag = true;
        int tmp;
        while (flag) {
            flag = false;
            for (int i = 0; i < set.length - 1; i++) {
                if (set[i] > set[i + 1]) {
                    tmp = set[i];
                    set[i] = set[i + 1];
                    set [i + 1] = tmp;
                    flag = true;
                }
            }
        }
        return set;
    }

    public static void main(String[] args) {
//        int[] set = {2, 4, 1, 0, 15, 11, 14, 1};
        int[] set = getSet();
        for (int x : set) System.out.print(x + " ");
        System.out.println();
        set = bsort(set);
        for (int x : set) System.out.print(x + " ");

    }
}
