package java8.foreach;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a on 15.12.15.
 */
public class ForEach {
    public static void main(String[] args) {
        String[] text = {"hui", "pizda", "ebat", "blyad"};
        for(String word : text) System.out.println(word);

        List<String> list = new ArrayList<>();
        list.add("HUI");
        list.add("PISDA");
        list.add("BLYAD");
        list.forEach(System.out::println);

        List<Pojo> pojos = new ArrayList<>();
        Pojo pojo = new Pojo();
        pojo.setX(10);
        pojo.setS("POJO1");
        pojos.add(pojo);
        Pojo pojo2 = new Pojo();
        pojo2.setX(20);
        pojo2.setS("POJO2");
        pojos.add(pojo);


        pojos.forEach(System.out::println);
    }

}
