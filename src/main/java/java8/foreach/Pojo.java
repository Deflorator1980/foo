package java8.foreach;

/**
 * Created by a on 15.12.15.
 */
public class Pojo {
    int x;
    String s;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "x=" + x +
                ", s='" + s + '\'' +
                '}';
    }
}
