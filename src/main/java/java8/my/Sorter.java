package java8.my;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Data{
    String name;
    String post;
    String company;
    public Data(String name, String post, String company) {
        super();
        this.name = name;
        this.post = post;
        this.company = company;
    }
    public String getName() {
        return name;
    }
    public String getPost() {
        return post;
    }
    public String getCompany() {
        return company;
    }
    @Override
    public String toString() {
        return "Data [name=" + name + ", post=" + post + ", company=" + company
                + "]";
    }
}

public class Sorter {
    public static void main(String[] args) {
        ArrayList<Data> datas = new ArrayList<>();
        datas.add(new Data("name1","post2","company3"));
        datas.add(new Data("name2", "post3", "company1"));
        datas.add(new Data("name3", "post1", "company2"));

        Collections.sort(datas, (o1, o2) -> o1.getPost().compareTo(o2.getPost()));

        datas.forEach(System.out::println);

//        Collections.sort(datas, new Comparator<Data>() {
//            @Override
//            public int compare(Data o1, Data o2) {
//                return o1.getCompany().compareTo(o2.getCompany());
//            }
//        });
//        for(Data data : datas){
//            System.out.println(data);
//        }
    }
}