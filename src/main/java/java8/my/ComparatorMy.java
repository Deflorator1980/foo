package java8.my;

import java.util.*;

/**
 * Created by a on 30.11.15.
 */
public class ComparatorMy {
    public static void main(String[] args) {
        List<String> huis = new ArrayList<>();
        huis.add("Hui");
        huis.add("Pisda");
        huis.add("Blyad");
        huis.add("Ebat");

        System.out.println(huis);

//        Collections.sort(huis, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.compareTo(o2);
//            }
//        });
//        Collections.sort(huis, ((o1, o2) -> o1.compareTo(o2)));

        String[] huiArray = huis.toArray(new String[huis.size()]);

        for(String s : huiArray) System.out.print(s + " ");
        System.out.println();

        Arrays.sort(huiArray);
        for(String s : huiArray) System.out.print(s + " ");

    }
}
