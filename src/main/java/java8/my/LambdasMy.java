package java8.my;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;


/**
 * Created by a on 23.12.15.
 */
public class LambdasMy {
    public static void main(String[] args) {

//        double penetration = Math.pow(4.9, 4);
//        System.out.println(penetration);
//        int a = (int)Math.round(penetration);
//        System.out.println(a);


//        Hui hui = text -> System.out.println(text);
//        hui.show("sosat");

//        Mat add = (int x, int y) -> x + y;
//        System.out.println(add.oper(3, 4));

//        BigDecimal xbd = new BigDecimal("4.9");
//        BigDecimal ybd = new BigDecimal("1.5");

//        MatBigDec powBd = (BigDecimal xdb, int y) -> xdb.pow(y);
//        System.out.println(powBd.operBigDec(new BigDecimal("4.9"), 5));


        BigDecimal a = new BigDecimal("4.9");
        BigDecimal b = new BigDecimal("0.5");


        MatDouble pow = (double xd, double yd) -> Math.pow(xd, yd);
        System.out.println(pow.operDouble(4.9, 0.5));



        BigDecimal pi = new BigDecimal("3.14");
        BigDecimal xe = new BigDecimal("2.5");

        final int SCALE = 100;
        BigDecimal x = new BigDecimal(1);
        BigDecimal y = new BigDecimal(12);

        BigDecimal z = BigFunctions.exp( BigFunctions.ln(pi, SCALE).multiply(xe),SCALE );
        System.out.println(z);



    }
    interface Hui{
        void show(String text);
    }
    interface Mat{
        int oper (int x, int y);
    }

    interface MatDouble {
        double operDouble (double x, double y);
    }
}
