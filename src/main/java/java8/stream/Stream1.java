package java8.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Stream1 {
    public static void main(String[] args) {
        List<Person> persons = Arrays.asList(
                new Person("And", 20),
                new Person("Igo", 23),
                new Person("Ira", 23),
                new Person("Vit", 12)
        );

        Map<Integer, List<Person>> personsByAge = persons
                .stream()
                .collect(Collectors.groupingBy(p -> p.id));

        personsByAge
                .forEach((id, p) -> System.out.format("age %s: %s\n", id, p));

        List<Person> filtered =
                persons
                        .stream()
                        .filter(p -> p.name.startsWith("I"))
                        .collect(Collectors.toList());

        System.out.println(filtered);
//        persons.forEach(System.out::println);



//*******************************************************************
//        Supplier<Stream<String>> streamSupplier =
//                () -> Stream.of("dd2", "aa2", "bb1", "bb3", "cc")
//                        .filter(s -> s.startsWith("a"));
//        streamSupplier.get().anyMatch(s -> true);
//        streamSupplier.get().noneMatch(s -> true);


//        Stream<String> stream =
//                Stream.of("dd2", "aa2", "bb1", "bb3", "cc")
//                        .filter(s -> s.startsWith("a"));
//
//        stream.anyMatch(s -> true);    // операция выполнится успешно
//        stream.noneMatch(s -> true);   // Вылетит Exception


//                Stream.of("dd2", "aa2", "bb4", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("b");
//                })
//                .sorted((s1, s2) -> {
//                    System.out.printf("sort: %s; %s\n", s1, s2);
//                    return s1.compareTo(s2);
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));


//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .sorted((s1, s2) -> {
//                    System.out.printf("sort: %s; %s\n", s1, s2);
//                    return s1.compareTo(s2);
//                })
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("a");
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));

//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("b");
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));

//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .anyMatch(s -> {
//                    System.out.println("anyMatch: " + s);
//                    return s.startsWith("A");
//                });

//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("Filter " + s);
//                    return true;
//                })
//                .forEach(s -> System.out.println("Print forEach " + s));

//        Stream.of(1.0, 2.0, 3.0)
////                .mapToInt(Double::intValue)
////                .mapToObj(i -> "hui" + i)
//                .forEach(System.out::println);


//        IntStream.range(1, 5)
//                .mapToObj(i -> "hui" + i)
//                .forEach(System.out::println);


//        Stream.of("c19","c28","c37")
//                .map(s -> s.substring(2))
//                .mapToInt(Integer::parseInt)
//                .max()
//                .ifPresent(System.out::print);

//        Arrays.stream(new int[] {1,2,3})
//                .map(x -> 2 * x +1)
//                .average()
//                .ifPresent(System.out::print);

//        IntStream.range(0, 10).forEach(System.out::println);

//        Stream.of("cc1", "cc2", "cc3")
//                .findFirst()
//                .map(String::toUpperCase)
//                .ifPresent(System.out::print);


//        Arrays.asList("cc1", "cc2", "cc3")
//                .stream()
//                .findFirst()
//                .ifPresent(System.out::print);

//        List<String> mList = Arrays.asList("aa1", "cc2", "cc1", "aa2", "bb1");
//        mList
//                .stream()
//                .filter(s -> s.startsWith("a"))
//                .map(String::toUpperCase)
//                .sorted()
//                .forEach(System.out::println);
//
//        String st = "sdfdsf";
//        System.out.println(st.startsWith("s"));

    }

}
