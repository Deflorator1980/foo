package collections;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;


public class ArrayOperations {
    public static void main(String[] args) {
        String[] text = {"hui", "pisda", "blyad", "ebat"};
        String[] text2 = text;


        for(String word : text) System.out.print(word + " ");
        System.out.println();

        Arrays.sort(text2);
        for(String word : text2) System.out.print(word + " ");
        System.out.println();

        String[] text3 = (String[]) ArrayUtils.addAll(text, text2);

        for(String word : text3) System.out.print(word + " ");


    }
}
