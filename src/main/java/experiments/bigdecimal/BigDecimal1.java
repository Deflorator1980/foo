package experiments.bigdecimal;

import java.math.BigDecimal;

/**
 * Created by a on 31.10.15.
 */
public class BigDecimal1 {
    public static void main(String[] args) {

        final int SCALE = 100;
        BigDecimal x = new BigDecimal(3.14);
        BigDecimal y = new BigDecimal(2.72);
        System.out.println(pow(x, y));
        System.out.println(Math.pow(3.14, 2.72));

        System.out.println(fac(new BigDecimal("50")));

        BigDecimal o1 = new BigDecimal("0.1");
        BigDecimal o2 = new BigDecimal("0.2");
        System.out.println(o1.add(o2));

        System.out.println((new BigDecimal("0.2").add(new BigDecimal("0.1"))));
    }

    public static BigDecimal pow(BigDecimal x, BigDecimal y) {
        final int SCALE = 100;
        return BigFunctions.exp( BigFunctions.ln(x, SCALE).multiply(y), SCALE);
    }
    public static BigDecimal fac (BigDecimal x){
        return (x.equals(BigDecimal.ZERO))? BigDecimal.ONE : x.multiply(fac(x.subtract(BigDecimal.ONE)));
    }
}
